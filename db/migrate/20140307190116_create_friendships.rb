class CreateFriendships < ActiveRecord::Migration
  def change
    create_table :friendships do |t|
      t.integer :origin_id
      t.integer :target_id

      t.timestamps
    end
    add_index :friendships, :origin_id
    add_index :friendships, :target_id
  end
end
