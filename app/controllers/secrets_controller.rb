class SecretsController < ApplicationController
  def new
    @secret = Secret.new
    @secret.author_id = current_user.id
    @secret.recipient_id = params[:user_id]
    render :new
  end

  def create
    @secret = Secret.new(secret_params)
    @secret.author_id = current_user.id
    @secret.recipient_id = params[:user_id]
    if @secret.save
      results = [@secret, @secret.tags]
      render :json => results, :status => 200
    else
      render :json => @secret.errors.full_messages, :status => 422
    end
  end

  private
  def secret_params
    params.require(:secret).permit(:title, :tag_ids => [])
  end
end
