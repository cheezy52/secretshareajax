class FriendshipsController < ApplicationController
  def create
    @friendship = Friendship.new
    @friendship.origin_id = current_user.id
    @friendship.target_id = params[:user_id]
    @friendship.save!
    render :json => @friendship, status: 200
  end

  def destroy
    @friendship = Friendship.find(params[:id])
    if @friendship.nil?
      puts params
      head 404
    else
      @friendship.destroy!
      head 200
    end
  end
end
