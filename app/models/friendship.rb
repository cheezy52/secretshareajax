class Friendship < ActiveRecord::Base
  validates :origin, :target, presence: true
  validates_uniqueness_of :target, :scope => :origin

  belongs_to :origin, :class_name => "User"
  belongs_to :target, :class_name => "User"

  def self.can_friend?(origin_id, target_id)
    (origin_id != target_id) &&
      (!Friendship.exists?(origin_id: origin_id, target_id: target_id))
  end
end
